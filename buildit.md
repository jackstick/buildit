FORMAT: 1A
HOST: http://192.168.99.100:3000

# ESI14-Buildit
Excerpt of RentIt's API

# Group invoice

## Invoice [/phr/{id}/invoice]

### post invoice [POST]

+ Parameters
    + id: 0 (number) - Invoice id

+ Request (application/json)

    {
        "period":{
            "startDate":"2016-12-20",
            "endDate":"2016-12-31"
        },
        "price":3000,
        "_xlinks": [
            {
                "href":"http://192.168.99.100:3000/api/sales/orders/100/remittance",
                "_rel":"remittance",
                "method":"POST"
            }
        ]
    }

+ Response 200 (application/json)

### set invoice late [PATCH]

+ Parameters
    + id: 0 (number) - Invoice id

+ Response 200 (application/json)