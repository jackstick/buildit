package com.buildit;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.application.exceptions.PODoesntExistException;
import com.buildit.common.application.exceptions.PlantNotAvailableException;
import com.buildit.common.domain.model.Approval;
import com.buildit.rental.application.dto.PlantHireRequestDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.dto.PurchaseOrderDTO;
import com.buildit.rental.application.service.RentalService;
import com.buildit.rental.domain.model.POStatus;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { TestContext.class, BuildItApplication.class })
@WebAppConfiguration
@ActiveProfiles("test")
public class practice7tests {
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	RentalService rentalService;

	@Test
	public void testCreatePO() throws PlantNotAvailableException {
		PlantInventoryEntryDTO plant = new PlantInventoryEntryDTO();
		plant.add(new Link("http://rentit.com/api/inventory/plants/13"));

		PurchaseOrderDTO order = new PurchaseOrderDTO();
		order.setPlant(plant);
		order.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(4)));

		PurchaseOrderDTO ret = rentalService.createPurchaseOrder(order);
		if (ret == null) {
			assertThat(1, is(0));
		}
	}

	List<ClientHttpRequestInterceptor> savedInterceptors = null;

	@Before
	public void setup() {
		savedInterceptors = restTemplate.getInterceptors();
	}

	@After
	public void tearoff() {
		restTemplate.setInterceptors(savedInterceptors);
	}

	@Test
	public void testRejection() throws RestClientException, IOException, PODoesntExistException {
		ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
					throws IOException {
				request.getHeaders().add("prefer", "409");
				return execution.execute(request, body);
			}
		};
		restTemplate.setInterceptors(Collections.singletonList(interceptor));

		PurchaseOrderDTO order = new PurchaseOrderDTO();
		order.add(new Link("http://192.168.99.100:3000/api/sales/orders/100"));
		try{
			rentalService.fetchPurchaseOrder(order);
			assertThat(1,is(0));
		} catch(PODoesntExistException e) {
			assertThat(1, is(1));
		} catch(Exception e) {
			assertThat(2, is(0));
		}
	}
	
	// ------------------------------------------------
	// HOMEWORK
	// ------------------------------------------------

	
	@Test
	public void testCreatePHR() {
	    //Query the plants (call to Api-Mock)
		PlantInventoryEntryDTO[] l = rentalService.getAllPlants();
		if (l.length == 0) {
			assertThat(0, is(1));
		}
		
	    //Choose any plant from the returned list of plants
		PlantInventoryEntryDTO plant = l[0];

	    //Create a PlantHireRequest
		PlantHireRequestDTO req = new PlantHireRequestDTO();
		req.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(4)));
		req.setPrice(plant.getPrice());
		req.setSiteEngineer("Toivo");
		req.setConstructionSite("Fake st. 123");
		req.setSupplier("Wise guys inc.");
		req.setPlant(plant);
		req.setStatus(Approval.APPROVED);

	    //Approve the PlantHireRequest (call to Api-Mock to create the purchase order)
		try{
			PurchaseOrderDTO po = rentalService.approvePHR(req);
			if (po == null) {
				assertThat(0, is(2));
			}
			if(po.getStatus() != POStatus.APPROVED) {
				assertThat(0, is(3));
			}
		} catch(Exception e) {
			e.printStackTrace();
			assertThat(0, is(4));
		}
	}
	
	@Before
	public void setup2() {
		savedInterceptors = restTemplate.getInterceptors();
	}

	@After
	public void tearoff2() {
		restTemplate.setInterceptors(savedInterceptors);
	}
	
	@Test
	public void testRejectCreatePHR() {
		ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
					throws IOException {
				request.getHeaders().add("prefer", "206");
				return execution.execute(request, body);
			}
		};
		restTemplate.setInterceptors(Collections.singletonList(interceptor));

		//Query the plants (call to Api-Mock)
		PlantInventoryEntryDTO[] l = rentalService.getAllPlants();
		if (l.length == 0) {
			assertThat(0, is(1));
		}
		
	    //Choose any plant from the returned list of plants
		PlantInventoryEntryDTO plant = l[0];

	    //Create a PlantHireRequest
		PlantHireRequestDTO req = new PlantHireRequestDTO();
		req.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(4)));
		req.setPrice(plant.getPrice());
		req.setSiteEngineer("Toivo");
		req.setConstructionSite("Fake st. 123");
		req.setSupplier("Wise guys inc.");
		req.setPlant(plant);
		req.setStatus(Approval.APPROVED);

	    //Approve the PlantHireRequest (call to Api-Mock to create the purchase order)
		try{
			PurchaseOrderDTO po = rentalService.approvePHR(req);
			if (po == null) {
				assertThat(0, is(1));
			}
			if (po.getStatus() != POStatus.REJECTED) {
				assertThat(0, is(2));
			}
		} catch(Exception e) {
			e.printStackTrace();
			assertThat(0, is(3));
		}
	}
	
	@Test
	public void testUpdateRejectedPOAccept() {
		savedInterceptors = restTemplate.getInterceptors();
		ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
					throws IOException {
				request.getHeaders().add("prefer", "206");
				return execution.execute(request, body);
			}
		};
		restTemplate.setInterceptors(Collections.singletonList(interceptor));

		//Query the plants (call to Api-Mock)
		PlantInventoryEntryDTO[] l = rentalService.getAllPlants();
		if (l.length == 0) {
			assertThat(0, is(1));
		}
		
	    //Choose any plant from the returned list of plants
		PlantInventoryEntryDTO plant = l[0];

	    //Create a PlantHireRequest
		PlantHireRequestDTO req = new PlantHireRequestDTO();
		req.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(4)));
		req.setPrice(plant.getPrice());
		req.setSiteEngineer("Toivo");
		req.setConstructionSite("Fake st. 123");
		req.setSupplier("Wise guys inc.");
		req.setPlant(plant);
		req.setStatus(Approval.APPROVED);

	    //Approve the PlantHireRequest (call to Api-Mock to create the purchase order)
		PurchaseOrderDTO po = null;
		try{
			po = rentalService.approvePHR(req);
			if (po.getStatus() != POStatus.REJECTED) {
				assertThat(0, is(2));
			}
		} catch(Exception e) {
			assertThat(0, is(3));
		}
		restTemplate.setInterceptors(savedInterceptors);
		
		req.setPrice(new BigDecimal("12500"));
        req.add(new Link(po.getLinks().get(0).getHref(), "po"));

		po = rentalService.updatePHR(req);
		assertThat(po.getStatus(),is(POStatus.APPROVED));
	}
	
	@Test
	public void testUpdateRejectedPOReject() {
		savedInterceptors = restTemplate.getInterceptors();
		ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
					throws IOException {
				request.getHeaders().add("prefer", "206");
				return execution.execute(request, body);
			}
		};
		restTemplate.setInterceptors(Collections.singletonList(interceptor));

		//Query the plants (call to Api-Mock)
		PlantInventoryEntryDTO[] l = rentalService.getAllPlants();
		if (l.length == 0) {
			assertThat(0, is(1));
		}
		
	    //Choose any plant from the returned list of plants
		PlantInventoryEntryDTO plant = l[0];

	    //Create a PlantHireRequest
		PlantHireRequestDTO req = new PlantHireRequestDTO();
		req.setRentalPeriod(BusinessPeriodDTO.of(LocalDate.now(), LocalDate.now().plusDays(4)));
		req.setPrice(plant.getPrice());
		req.setSiteEngineer("Toivo");
		req.setConstructionSite("Fake st. 123");
		req.setSupplier("Wise guys inc.");
		req.setPlant(plant);
		req.setStatus(Approval.APPROVED);

	    //Approve the PlantHireRequest (call to Api-Mock to create the purchase order)
		PurchaseOrderDTO po = null;
		try{
			po = rentalService.approvePHR(req);
			if (po.getStatus() != POStatus.REJECTED) {
				assertThat(0, is(2));
			}
		} catch(Exception e) {
			assertThat(0, is(3));
		}
		
		req.setPrice(new BigDecimal("90"));
        req.add(new Link(po.getLinks().get(0).getHref(), "po"));
		
		po = rentalService.updatePHR(req);
		assertThat(po.getStatus(),is(POStatus.REJECTED));
		
		restTemplate.setInterceptors(savedInterceptors);
	}
}
