package com.buildit;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.domain.model.Approval;
import com.buildit.rental.application.dto.PlantHireRequestDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.dto.PurchaseOrderDTO;
import com.buildit.rental.application.service.PlantService;
import com.buildit.rental.application.service.RentalService;
import com.buildit.rental.domain.model.POStatus;
import com.buildit.rental.domain.model.PlantHireRequestID;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { TestContext.class, BuildItApplication.class })
@WebAppConfiguration
@ActiveProfiles("test")
public class PlantHireRequestControllerTests {
	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Autowired
	@Qualifier("_halObjectMapper")
	ObjectMapper mapper;

	@Autowired
	RentalService rentalService;

	@Autowired
	PlantService plantService;

	@Before
	public void setup() {
		Mockito.reset(plantService);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void createPhrTest() throws Exception {
		String constructionSite = "Leningrad";
		String supplier = "Joonas Lonks";
		String siteEngineer = "Sander Tiga Nikk";
		BusinessPeriodDTO period = BusinessPeriodDTO.of(LocalDate.of(2016, 3, 31), LocalDate.of(2016, 4, 4));

		// Setting up plant mock..
		mockPlantListFromJson(period);

		// Querying plant list.
		MvcResult plantResult = mockMvc.perform(get("/phr/plants?name=Truck&startDate={start}&endDate={end}",
				period.getStartDate(), period.getEndDate())).andExpect(status().isOk()).andReturn();
		List<PlantInventoryEntryDTO> plants = mapper.readValue(plantResult.getResponse().getContentAsString(),
				new TypeReference<List<PlantInventoryEntryDTO>>() {
				});
		PlantInventoryEntryDTO plant = plants.get(0);

		PlantHireRequestDTO phrDto = PlantHireRequestDTO.of(null, period, null, siteEngineer, constructionSite,
				supplier, null, plant, null, null);

		// Creating the plant hire request.
		MvcResult result1 = mockMvc
				.perform(
						post("/phr").content(mapper.writeValueAsString(phrDto)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated()).andReturn();

		PlantHireRequestDTO phrResponseDto = mapper.readValue(result1.getResponse().getContentAsString(),
				new TypeReference<PlantHireRequestDTO>() {
				});
		
		// Setting up PO mock
		mockPO(period, plant, phrResponseDto.getPrice());
		
		// Accepting phr, which yields a PO automatically. Also pretend the officer approves PO immediately in this test.		
		PlantHireRequestDTO approveDto = PlantHireRequestDTO.of(null, null, null, null, null, null, null, null, null, null);
		approveDto.setStatus(Approval.APPROVED);
		approveDto.setComments("Didn't like the color, but it's OK.");
		
		MvcResult result2 = mockMvc.perform(patch(phrResponseDto.getLink("accept").getHref())
				.content(mapper.writeValueAsString(approveDto)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent()).andReturn();
		PurchaseOrderDTO po = mapper.readValue(result2.getResponse().getContentAsString(),
				new TypeReference<PurchaseOrderDTO>() {
				});
		
		// Check if PO is accepted.
		assertThat(po.getStatus(), is(POStatus.OPEN));
		
		// Checking state of PHR
		PlantHireRequestDTO phrLastState = rentalService.findPlantHireRequest(PlantHireRequestID.of(phrResponseDto.get_id()));
		assertThat(phrLastState.getStatus(), is(Approval.APPROVED));
	}

	private void mockPlantListFromJson(BusinessPeriodDTO period)
			throws JsonParseException, JsonMappingException, IOException {
		Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
		List<PlantInventoryEntryDTO> list = mapper.readValue(responseBody.getFile(),
				new TypeReference<List<PlantInventoryEntryDTO>>() {
				});
		Mockito.when(plantService.findAvailablePlants("Truck", period.getStartDate(), period.getEndDate()))
				.thenReturn(list);
	}

	private void mockPO(BusinessPeriodDTO rentalPeriod, PlantInventoryEntryDTO plant, BigDecimal total)
			throws JsonParseException, JsonMappingException, IOException {
		Mockito.when(plantService.findPlant(plant.getLinks().get(0).getHref())).thenReturn(plant);
	}

}