package com.buildit;

import com.buildit.rental.application.service.PlantService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
public class TestContext {
    @Bean
    public PlantService plantService() {
        return Mockito.mock(PlantService.class);
    }
}

