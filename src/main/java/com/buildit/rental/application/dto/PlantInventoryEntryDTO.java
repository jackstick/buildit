package com.buildit.rental.application.dto;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.Link;

import com.buildit.common.rest.ResourceSupport;

import java.math.BigDecimal;

@Data
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@AllArgsConstructor(staticName = "of")
public class PlantInventoryEntryDTO extends ResourceSupport {
    Long _id;
    String name;
    String description;
    BigDecimal price;
    
	public PlantInventoryEntryDTO(String href) {
		super();
		this.add(new Link(href));
	}
    
    
}