package com.buildit.rental.application.dto;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.domain.model.Approval;
import com.buildit.common.rest.ResourceSupport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor(staticName="of")
@NoArgsConstructor(force = true)
public class PlantHireRequestDTO extends ResourceSupport {
    Long _id;
    BusinessPeriodDTO rentalPeriod;
    BigDecimal price;
        
	String siteEngineer;
	String constructionSite;
	String supplier;
	
	String comments;
	
	PlantInventoryEntryDTO plant;
	
	Approval status;
	
	String po;
}