package com.buildit.rental.application.dto;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.domain.model.Approval;
import com.buildit.common.domain.model.BusinessPeriod;
import com.buildit.common.rest.ResourceSupport;
import com.buildit.rental.domain.model.InvoiceID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import javax.persistence.EmbeddedId;

import org.springframework.hateoas.Link;

@Data
@AllArgsConstructor(staticName="of")
@NoArgsConstructor(force = true)
public class InvoiceDTO extends ResourceSupport {
    Long _id;

	BusinessPeriodDTO period;
	BigDecimal price;
	
	Approval status;
	
	Boolean late;
	
	String poUrl;

}