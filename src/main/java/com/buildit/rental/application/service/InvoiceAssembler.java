package com.buildit.rental.application.service;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.rest.ExtendedLink;
import com.buildit.rental.application.dto.InvoiceDTO;
import com.buildit.rental.domain.model.Invoice;
import com.buildit.rental.rest.InvoiceRestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Service
public class InvoiceAssembler extends ResourceAssemblerSupport<Invoice, InvoiceDTO> {
	
	@Autowired
	RentalService rentalService;

	@Autowired
	PlantService plantService;
	
	@Autowired
	InvoiceService invoiceService;

	public InvoiceAssembler() {
		super(InvoiceRestController.class, InvoiceDTO.class);
	}

	@Override
	public InvoiceDTO toResource(Invoice invoice) {
		InvoiceDTO dto = createResourceWithId(invoice.getId().getId(), invoice);
		dto.set_id(invoice.getId().getId());
		dto.setStatus(invoice.getStatus());

		LocalDate startDate = invoice.getPeriod().getStartDate();
		LocalDate endDate = invoice.getPeriod().getEndDate();
		dto.setPeriod(BusinessPeriodDTO.of(startDate, endDate));
		dto.setPrice(invoice.getPrice());
		dto.setLate(invoice.getLate());
		dto.setPoUrl(invoice.getPhr().getPoHref());

		try {
			switch (invoice.getStatus()) {
			case PENDING:
				dto.add(new ExtendedLink(linkTo(
						methodOn(InvoiceRestController.class).assessInvoice(null, dto.get_id(), null))
								.toString(),
						"accept", org.springframework.http.HttpMethod.PATCH));
				dto.add(new ExtendedLink(linkTo(
						methodOn(InvoiceRestController.class).assessInvoice(null, dto.get_id(), null))
								.toString(),
						"reject", org.springframework.http.HttpMethod.PATCH));
				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dto;
	}
}
