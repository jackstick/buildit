package com.buildit.rental.application.service;

import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.dto.PurchaseOrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Service
public class PlantService {

	@Autowired
	RestTemplate restTemplate;
	
	@Value("${rentit.host:}")
	String host;

	@Value("${rentit.port:}")
	String port;

	public List<PlantInventoryEntryDTO> findAvailablePlants(String plantName, LocalDate startDate, LocalDate endDate) {
		PlantInventoryEntryDTO[] plants = restTemplate.getForObject(
				"http://localhost:8090/api/inventory/plants?name={name}&startDate={start}&endDate={end}",
				PlantInventoryEntryDTO[].class, plantName, startDate, endDate);
		return Arrays.asList(plants);
	}

	public PlantInventoryEntryDTO findPlant(String href) {
		PlantInventoryEntryDTO plant = restTemplate.getForObject(href,
				PlantInventoryEntryDTO.class);
		return plant;
	}

}
