package com.buildit.rental.application.service;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.application.exceptions.PODoesntExistException;
import com.buildit.common.application.exceptions.PlantNotAvailableException;
import com.buildit.common.domain.model.Approval;
import com.buildit.common.domain.model.BusinessPeriod;
import com.buildit.rental.application.dto.InvoiceDTO;
import com.buildit.rental.application.dto.PlantHireRequestDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.dto.PurchaseOrderDTO;
import com.buildit.rental.domain.model.PlantHireRequest;
import com.buildit.rental.domain.model.PlantHireRequestID;
import com.buildit.rental.domain.repository.InvoiceRepository;
import com.buildit.rental.domain.repository.PlantHireRequestRepository;
import com.buildit.rental.infrastructure.idgeneration.RentalIdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Service
public class RentalService {

	@Autowired
	PlantHireRequestAssembler plantHireRequestAssembler;

	@Autowired
	PlantHireRequestRepository plantHireRequestRepository;
	
	@Autowired
	InvoiceAssembler invoiceAssembler;
	
	@Autowired
	InvoiceRepository invoiceRepository;

	@Autowired
	RentalIdentifierGenerator idGenerator;

	@Autowired
	RestTemplate restTemplate;

	@Value("${rentit.host:}")
	String host;

	@Value("${rentit.port:}")
	String port;

	public PurchaseOrderDTO createPurchaseOrder(PurchaseOrderDTO order) throws PlantNotAvailableException {
		try {
			ResponseEntity<PurchaseOrderDTO> result = restTemplate
					.postForEntity("http://" + host + ":" + port + "/api/sales/orders", order, PurchaseOrderDTO.class);
			return result.getBody();
		} catch (final HttpClientErrorException e) {
			if (e.getStatusCode().equals(HttpStatus.CONFLICT)) {
				throw new PlantNotAvailableException("Plant no longer available");
			}
			throw e;
		}
	}
	
	public PurchaseOrderDTO fetchPurchaseOrder(PurchaseOrderDTO order) throws PODoesntExistException {
		try {
			ResponseEntity<PurchaseOrderDTO> result = restTemplate
					.getForEntity(order.getLinks().get(0).getHref(), PurchaseOrderDTO.class);
			return result.getBody();
		} catch (final HttpClientErrorException e) {
			if (e.getStatusCode().equals(HttpStatus.CONFLICT)) {
				throw new PODoesntExistException("Purchase order not existant");
			}
		}
		return null;
	}
	
	public PlantInventoryEntryDTO[] getAllPlants() {
		try {
			ResponseEntity<PlantInventoryEntryDTO[]> result = restTemplate
					.getForEntity("http://" + host + ":" + port + "/api/inventory/plants", PlantInventoryEntryDTO[].class);
			return result.getBody();
		} catch (final HttpClientErrorException e) {}
		return null;
	}
	
	public PurchaseOrderDTO approvePHR(PlantHireRequestDTO req) throws Exception {
		ResponseEntity<PurchaseOrderDTO> result = null;
		try {
			 result = restTemplate
					.postForEntity("http://" + host + ":" + port + "/api/rental/check", req, PurchaseOrderDTO.class);
			return result.getBody();
		} catch (final HttpClientErrorException e) {
			throw e;
		}
	}
	
	public PurchaseOrderDTO updatePHR(PlantHireRequestDTO req) {
		ResponseEntity<PurchaseOrderDTO> result = null;
		try {
			 result = restTemplate
					.postForEntity("http://" + host + ":" + port + "/api/rental/update", req, PurchaseOrderDTO.class);
			return result.getBody();
		} catch (final HttpClientErrorException e) {
			throw e;
		}
	}

	public PlantHireRequestDTO findPlantHireRequest(PlantHireRequestID id) {
		return plantHireRequestAssembler.toResource(plantHireRequestRepository.findOne(id));
	}

	public PlantHireRequestDTO createPlantHireRequest(PlantHireRequestDTO dto) {
		PlantHireRequestID id = idGenerator.nextPlantHireRequestID();
		String comments = null;

		LocalDate startDate = dto.getRentalPeriod().getStartDate();
		LocalDate endDate = dto.getRentalPeriod().getEndDate();
		BusinessPeriod period = BusinessPeriod.of(startDate, endDate);

		BigDecimal price = sumPrice(dto.getPlant().getPrice(), period);
		Approval approved = Approval.PENDING;

		String siteEngineer = dto.getSiteEngineer();
		String constructionSite = dto.getConstructionSite();
		String supplier = dto.getSupplier();

		String plant = dto.getPlant().getLink("self").getHref();

		PlantHireRequest request = PlantHireRequest.of(id, price, comments, approved, period, siteEngineer,
				constructionSite, supplier, plant, null);
		PlantHireRequest saved = plantHireRequestRepository.save(request);
		return plantHireRequestAssembler.toResource(saved);
	}

	public PlantHireRequestDTO modifyPlantHireRequest(PlantHireRequestDTO dto, Long id) {
		PlantHireRequest request = plantHireRequestRepository.findOne(PlantHireRequestID.of(id));

		LocalDate startDate = dto.getRentalPeriod().getStartDate();
		LocalDate endDate = dto.getRentalPeriod().getEndDate();
		BusinessPeriod period = BusinessPeriod.of(startDate, endDate);

		request.setComments(dto.getComments());
		request.setRentalPeriod(period);
		request.setStatus(Approval.PENDING);
		request.setConstructionSite(dto.getConstructionSite());
		request.setTotalPrice(sumPrice(dto.getPlant().getPrice(), period));
		request.setSiteEngineer(dto.getSiteEngineer());
		request.setSupplier(dto.getSupplier());

		String plant = dto.getPlant().getLink("self").getHref();
		request.setPlantHref(plant);

		plantHireRequestRepository.save(request);

		return plantHireRequestAssembler.toResource(request);
	}

	public void cancelPlantHireRequest(Long id) {
		PlantHireRequestID plant = PlantHireRequestID.of(id);
		plantHireRequestRepository.delete(plant);
	}

	public PlantHireRequestDTO checkPlantHireRequestStatus(Long id) {
		PlantHireRequest request = plantHireRequestRepository.findOne(PlantHireRequestID.of(id));
		return PlantHireRequestDTO.of(null, null, null, null, null, null, null, null, request.getStatus(), null);
	}

	public PlantHireRequestDTO assessPlantHireRequest(PlantHireRequestDTO dto, Long id) {
		PlantHireRequest request = plantHireRequestRepository.findOne(PlantHireRequestID.of(id));
		request.setStatus(dto.getStatus());
		request.setComments(dto.getComments());
		plantHireRequestRepository.save(request);
		return plantHireRequestAssembler.toResource(request);
	}

	private static BigDecimal sumPrice(BigDecimal perDay, BusinessPeriod period) {
		return perDay.multiply(new BigDecimal(period.numberOfWorkingDays()));
	}

	public void associatePOtoPHR(PurchaseOrderDTO poDto, Long id) {
		PlantHireRequest request = plantHireRequestRepository.findOne(PlantHireRequestID.of(id));
		request.setPoHref(poDto.getLink("self").getHref());
		plantHireRequestRepository.save(request);
	}

	public List<PlantHireRequestDTO> findAllPHR() {
		return plantHireRequestAssembler.toResources(plantHireRequestRepository.findAll());
	}

	public String extendPhr(Long id, BusinessPeriod period) {
		PlantHireRequest phr = plantHireRequestRepository.findOne(PlantHireRequestID.of(id));
		
		BusinessPeriod bp = phr.getRentalPeriod();
    	phr.setRentalPeriod(BusinessPeriod.of(phr.getRentalPeriod().getStartDate(), period.getEndDate()));
    	
    	BigDecimal oneDay = phr.getTotalPrice().divide(new BigDecimal(bp.numberOfWorkingDays()));
    	phr.setTotalPrice(oneDay.multiply(new BigDecimal(phr.getRentalPeriod().numberOfWorkingDays())));
    	
    	plantHireRequestRepository.save(phr);
    	
    	return phr.getTotalPrice().toString();
	}

}