package com.buildit.rental.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.domain.model.Approval;
import com.buildit.common.domain.model.BusinessPeriod;
import com.buildit.rental.application.dto.InvoiceDTO;
import com.buildit.rental.domain.model.Invoice;
import com.buildit.rental.domain.model.InvoiceID;
import com.buildit.rental.domain.model.PlantHireRequest;
import com.buildit.rental.domain.model.PlantHireRequestID;
import com.buildit.rental.domain.repository.InvoiceRepository;
import com.buildit.rental.domain.repository.PlantHireRequestRepository;
import com.buildit.rental.infrastructure.idgeneration.RentalIdentifierGenerator;

@Service
public class InvoiceService {

	@Autowired
	PlantHireRequestAssembler plantHireRequestAssembler;

	@Autowired
	PlantHireRequestRepository plantHireRequestRepository;

	@Autowired
	InvoiceAssembler invoiceAssembler;

	@Autowired
	InvoiceRepository invoiceRepository;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	RentalIdentifierGenerator idGenerator;

	@Value("${rentit.host:}")
	String host;

	@Value("${rentit.port:}")
	String port;

	public List<InvoiceDTO> findAllInvoices() {
		return invoiceAssembler.toResources(invoiceRepository.findAll());
	}

	public void setLate(Long id) {
		try {
			Invoice invoice = invoiceRepository.findOne(InvoiceID.of(id));
			invoice.setLate(true);
			invoiceRepository.save(invoice);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createInvoice(Long id, InvoiceDTO invoiceDTO) {
		PlantHireRequest phr = plantHireRequestRepository.findOne(PlantHireRequestID.of(id));
		BusinessPeriodDTO dtoPeriod = invoiceDTO.getPeriod();
		BusinessPeriod period = BusinessPeriod.of(dtoPeriod.getStartDate(), dtoPeriod.getEndDate());

		/*
		 * If the price in invoice does not match the cost in PHR, the invoice
		 * is rejected automatically.
		 */
		Approval approval;
		if (phr.getTotalPrice().compareTo(invoiceDTO.getPrice()) == 0)
			approval = Approval.PENDING;
		else
			approval = Approval.REJECTED;

		String remittanceUrl = invoiceDTO.get_xlink("remittance").getHref();
		Invoice i = Invoice.of(idGenerator.nextInvoiceID(), period, invoiceDTO.getPrice(), approval, false, phr,
				remittanceUrl);
		invoiceRepository.save(i);
	}

	public InvoiceDTO assessInvoice(Long id, Approval status) {
		try {
			Invoice invoice = invoiceRepository.findOne(InvoiceID.of(id));
			invoice.setStatus(status);
			if (status == Approval.APPROVED) {
				sendRemittance(invoice.getRemittanceUrl());
			}
			invoiceRepository.save(invoice);
			return invoiceAssembler.toResource(invoice);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void sendRemittance(String remittanceUrl) {
		restTemplate.postForEntity(remittanceUrl, null, HttpStatus.class);
	}

	public boolean validateInvoice(Long id, InvoiceDTO invoiceDTO) {
		try {
			PlantHireRequest phr = plantHireRequestRepository.findOne(PlantHireRequestID.of(id));
			return phr.getPoHref().equals(invoiceDTO.getPoUrl());
		} catch (Exception e) {
			return false;
		}
	}

}