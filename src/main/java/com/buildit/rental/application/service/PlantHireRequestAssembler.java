package com.buildit.rental.application.service;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.rest.ExtendedLink;
import com.buildit.rental.application.dto.PlantHireRequestDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.domain.model.PlantHireRequest;
import com.buildit.rental.rest.PlantHireRequestRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.*;

@Service
public class PlantHireRequestAssembler extends ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestDTO> {
	@Autowired
	RentalService rentalService;

	@Autowired
	PlantService plantService;

	public PlantHireRequestAssembler() {
		super(PlantHireRequestRestController.class, PlantHireRequestDTO.class);
	}

	@Override
	public PlantHireRequestDTO toResource(PlantHireRequest plantHireRequest) {
		PlantHireRequestDTO dto = createResourceWithId(plantHireRequest.getId().getId(), plantHireRequest);
		dto.set_id(plantHireRequest.getId().getId());
		dto.setStatus(plantHireRequest.getStatus());
		dto.setComments(plantHireRequest.getComments());
		dto.setConstructionSite(plantHireRequest.getConstructionSite());

		LocalDate startDate = plantHireRequest.getRentalPeriod().getStartDate();
		LocalDate endDate = plantHireRequest.getRentalPeriod().getEndDate();
		dto.setRentalPeriod(BusinessPeriodDTO.of(startDate, endDate));

		PlantInventoryEntryDTO plant = new PlantInventoryEntryDTO(plantHireRequest.getPlantHref());
		dto.setPlant(plant);

		dto.setPrice(plantHireRequest.getTotalPrice());
		dto.setSiteEngineer(plantHireRequest.getSiteEngineer());
		dto.setSupplier(plantHireRequest.getSupplier());
		dto.setPo(plantHireRequest.getPoHref());

		try {
			switch (plantHireRequest.getStatus()) {
			case PENDING:
				dto.add(new ExtendedLink(linkTo(
						methodOn(PlantHireRequestRestController.class).assessPlantHireRequest(null, dto.get_id(), null))
								.toString(),
						"reject", PATCH));
				dto.add(new ExtendedLink(linkTo(
						methodOn(PlantHireRequestRestController.class).assessPlantHireRequest(null, dto.get_id(), null))
								.toString(),
						"accept", PATCH));
				dto.add(new ExtendedLink(linkTo(
						methodOn(PlantHireRequestRestController.class).modifyPlantHireRequest(null, dto.get_id(), null))
								.toString(),
						"modify", PUT));
				dto.add(new ExtendedLink(linkTo(
						methodOn(PlantHireRequestRestController.class).cancelPlantHireRequest(dto.get_id(), null))
								.toString(),
						"cancel", DELETE));
				break;
			case REJECTED:
				dto.add(new ExtendedLink(linkTo(
						methodOn(PlantHireRequestRestController.class).modifyPlantHireRequest(null, dto.get_id(), null))
								.toString(),
						"modify", PUT));
				break;
			case APPROVED:
				dto.add(new ExtendedLink(plantHireRequest.getPoHref()+"/extensions/",
						"extend", POST));
				break;
			default:
				break;
			}
		} catch (Exception e) {
		}

		return dto;
	}
}
