package com.buildit.rental.application.service;

import java.util.List;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import com.buildit.common.application.dto.QueryDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;

@MessagingGateway
public interface RentalServiceGateway {
	@Gateway(requestChannel = "queryPlantCatalogChannel")
	List<PlantInventoryEntryDTO> queryPlantCatalog(QueryDTO query);
}