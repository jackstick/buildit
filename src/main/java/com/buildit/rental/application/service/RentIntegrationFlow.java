package com.buildit.rental.application.service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.HttpMethod;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.dsl.support.Transformers;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;

import com.bazaarvoice.jolt.Chainr;
import com.bazaarvoice.jolt.JsonUtils;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RentIntegrationFlow {

	@Bean
	IntegrationFlow flow() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.registerModule(new Jackson2HalModule());
		return IntegrationFlows.from("queryPlantCatalogChannel")
				.publishSubscribeChannel(
						Executors
								.newCachedThreadPool(),
						s -> s.applySequence(true).subscribe(f -> f
								.handle(Http.outboundGateway("http://192.168.99.100:3030/rest/plants?name={name}")
										.uriVariable("name", "payload.name").httpMethod(HttpMethod.GET)
										.expectedResponseType(String.class))
								.<String,String>transform(m -> sirenToHal(m))
								.transform(Transformers.fromJson(PlantInventoryEntryDTO[].class,
										new Jackson2JsonObjectMapper(mapper)))
						.channel("queryPlantCatalogChannel-gather"))
						.subscribe(f -> f
								.handle(Http.outboundGateway("http://192.168.99.100:3000/rest/plants?name={name}")
										.uriVariable("name", "payload.name").httpMethod(HttpMethod.GET)
										.expectedResponseType(String.class))
								.transform(Transformers.fromJson(PlantInventoryEntryDTO[].class,
										new Jackson2JsonObjectMapper(mapper)))
								.channel("queryPlantCatalogChannel-gather")))
				.get();
	}

	@Bean
	IntegrationFlow gatherFlow() {
		return IntegrationFlows.from("queryPlantCatalogChannel-gather")
				.aggregate(a -> a.outputProcessor(g -> g.getMessages().stream()
						.flatMap(m -> Arrays.stream((PlantInventoryEntryDTO[]) m.getPayload()))
						.collect(Collectors.toList())))
				.get();
	}

	String sirenToHal(String input) {
		try {
			Resource spec = new ClassPathResource("specCollection.json", this.getClass());
			List<Object> objs = JsonUtils.jsonToList(spec.getInputStream());
			Chainr chainr = Chainr.fromSpec(objs);
			String result = JsonUtils.toJsonString(chainr.transform(JsonUtils.jsonToMap(input)));
			return result;
		} catch (Exception e) {
			return null;
		}
	}

}
