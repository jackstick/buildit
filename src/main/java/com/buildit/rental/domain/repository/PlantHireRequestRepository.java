package com.buildit.rental.domain.repository;

import com.buildit.rental.domain.model.PlantHireRequest;
import com.buildit.rental.domain.model.PlantHireRequestID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlantHireRequestRepository  extends JpaRepository<PlantHireRequest, PlantHireRequestID> {

}
