package com.buildit.rental.domain.repository;

import com.buildit.rental.domain.model.Invoice;
import com.buildit.rental.domain.model.InvoiceID;
import com.buildit.rental.domain.model.PlantHireRequest;
import com.buildit.rental.domain.model.PlantHireRequestID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceRepository  extends JpaRepository<Invoice, InvoiceID> {

}
