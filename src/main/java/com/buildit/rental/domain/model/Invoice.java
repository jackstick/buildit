package com.buildit.rental.domain.model;

import java.math.BigDecimal;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.buildit.common.domain.model.Approval;
import com.buildit.common.domain.model.BusinessPeriod;
import com.buildit.rental.application.dto.PurchaseOrderDTO;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@AllArgsConstructor(staticName = "of")
public class Invoice {

	@EmbeddedId
	InvoiceID id;
	BusinessPeriod period;
	BigDecimal price;
	
	Approval status;
	
	Boolean late;
	
	@OneToOne
	PlantHireRequest phr;
	
	String remittanceUrl;
}
