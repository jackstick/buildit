package com.buildit.rental.domain.model;

public enum POStatus {
	PENDING, OPEN, REJECTED, CLOSED, APPROVED
}
