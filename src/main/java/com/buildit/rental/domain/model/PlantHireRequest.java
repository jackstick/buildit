package com.buildit.rental.domain.model;

import com.buildit.common.domain.model.Approval;
import com.buildit.common.domain.model.BusinessPeriod;
import lombok.*;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
@AllArgsConstructor(staticName = "of")
public class PlantHireRequest {

	@EmbeddedId
	PlantHireRequestID id;
	BigDecimal totalPrice;
	String comments;
	Approval status;
	BusinessPeriod rentalPeriod;
	
	String siteEngineer;
	String constructionSite;
	String supplier;
	
	String plantHref;
	String poHref;

}
