package com.buildit.rental.infrastructure.idgeneration;

import com.buildit.common.infrastructure.HibernateBasedIdentifierGenerator;
import com.buildit.rental.domain.model.InvoiceID;
import com.buildit.rental.domain.model.PlantHireRequestID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RentalIdentifierGenerator {
	@Autowired
	HibernateBasedIdentifierGenerator hibernateGenerator;

	public PlantHireRequestID nextPlantHireRequestID() {
		return PlantHireRequestID.of(hibernateGenerator.getID("PlantHireRequestIDSequence"));
	}

	public InvoiceID nextInvoiceID() {
		return InvoiceID.of(hibernateGenerator.getID("InvoiceIDSequence"));
	}

}