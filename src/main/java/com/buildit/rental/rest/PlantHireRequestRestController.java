package com.buildit.rental.rest;

import com.buildit.auth.application.exceptions.UnauthorizedException;
import com.buildit.auth.application.service.AuthService;
import com.buildit.auth.domain.model.Role;
import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.domain.model.Approval;
import com.buildit.common.domain.model.BusinessPeriod;
import com.buildit.common.rest.ExtendedLink;
import com.buildit.rental.application.dto.InvoiceDTO;
import com.buildit.rental.application.dto.PlantHireRequestDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.dto.PurchaseOrderDTO;
import com.buildit.rental.application.service.InvoiceService;
import com.buildit.rental.application.service.PlantHireRequestAssembler;
import com.buildit.rental.application.service.PlantService;
import com.buildit.rental.application.service.RentalService;
import com.buildit.rental.domain.model.POStatus;
import com.buildit.rental.domain.model.PlantHireRequest;
import com.buildit.rental.domain.model.PlantHireRequestID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/phr")
public class PlantHireRequestRestController {

	@Autowired
	RentalService rentalService;

	@Autowired
	PlantService plantService;

	@Autowired
	InvoiceService invoiceService;

	@Autowired
	AuthService authService;

	@CrossOrigin
	@RequestMapping(method = GET, path = "/plants")
	@ResponseStatus(HttpStatus.OK)
	public List<PlantInventoryEntryDTO> findAvailablePlants(@RequestParam(name = "name") String plantName,
			@RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {

		return plantService.findAvailablePlants(plantName, startDate, endDate);
	}

	@CrossOrigin
	@RequestMapping(method = GET, path = "/{id}")
	public ResponseEntity<PlantHireRequestDTO> getPlantHireRequest(@PathVariable Long id) throws Exception {
		PlantHireRequestDTO dto = rentalService.findPlantHireRequest(PlantHireRequestID.of(id));
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PlantHireRequestDTO>(dto, headers, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(method = GET, path = "/")
	public ResponseEntity<List<PlantHireRequestDTO>> getPlantHireRequests() throws Exception {
		List<PlantHireRequestDTO> dto = rentalService.findAllPHR();
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<List<PlantHireRequestDTO>>(dto, headers, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(method = POST)
	public ResponseEntity<PlantHireRequestDTO> createPlantHireRequest(@RequestBody PlantHireRequestDTO phrDTO,
			@RequestHeader HttpHeaders request) throws Exception {
		authService.authenticateRequestForRole(request, Role.SITE_ENGINEER);
		PlantHireRequestDTO dto = rentalService.createPlantHireRequest(phrDTO);
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PlantHireRequestDTO>(dto, headers, HttpStatus.CREATED);
	}

	@CrossOrigin
	@RequestMapping(method = PATCH, path = "/{id}")
	public ResponseEntity<PlantHireRequestDTO> modifyPlantHireRequest(@RequestBody PlantHireRequestDTO phrDTO,
			@PathVariable Long id, @RequestHeader HttpHeaders request) throws Exception {
		authService.authenticateRequestForRole(request, Arrays.asList(Role.SITE_ENGINEER, Role.WORKS_ENGINEER));
		PlantHireRequestDTO phrLastState = rentalService.findPlantHireRequest(PlantHireRequestID.of(id));
		if (phrLastState.getStatus() == Approval.APPROVED) {
			throw new PHRAcceptedException(id);
		}
		PlantHireRequestDTO dto = rentalService.modifyPlantHireRequest(phrDTO, id);
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PlantHireRequestDTO>(dto, headers, HttpStatus.NO_CONTENT);

	}

	@CrossOrigin
	@RequestMapping(method = PATCH, path = "/{id}/extend")
	public ResponseEntity<String> extendPlantHireRequest(@RequestBody BusinessPeriod period, @PathVariable Long id,
			@RequestHeader HttpHeaders request) throws Exception {
		authService.authenticateRequestForRole(request, Role.SITE_ENGINEER);
		String price = rentalService.extendPhr(id, period);

		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<String>(price, headers, HttpStatus.OK);

	}

	@CrossOrigin
	@RequestMapping(method = POST, path = "/{id}/invoice")
	public ResponseEntity<?> createPlantHireRequestInvoice(@PathVariable Long id, @RequestBody InvoiceDTO invoiceDTO)
			throws Exception {
		if (!invoiceService.validateInvoice(id, invoiceDTO)) {
			throw new UnauthorizedException("Incorrect PO invoiced.");
		}
		invoiceService.createInvoice(id, invoiceDTO);
		return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);
	}

	@CrossOrigin
	@RequestMapping(method = PATCH, path = "/{id}/invoice")
	public ResponseEntity<?> setPlantHireRequestInvoiceLate(@PathVariable Long id) throws Exception {
		invoiceService.setLate(id);
		return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);
	}

	@CrossOrigin
	@RequestMapping(method = DELETE, path = "/{id}")
	public ResponseEntity<?> cancelPlantHireRequest(@PathVariable Long id, @RequestHeader HttpHeaders request)
			throws Exception {
		authService.authenticateRequestForRole(request, Role.SITE_ENGINEER);
		rentalService.cancelPlantHireRequest(id);
		return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);
	}

	@CrossOrigin
	@RequestMapping(method = GET, path = "/{id}/status")
	public ResponseEntity<PlantHireRequestDTO> getPlantHireRequestStatus(@PathVariable Long id) throws Exception {
		PlantHireRequestDTO dto = rentalService.checkPlantHireRequestStatus(id);
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PlantHireRequestDTO>(dto, headers, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(method = PATCH, path = "/{id}/status")
	public ResponseEntity<PurchaseOrderDTO> assessPlantHireRequest(@RequestBody PlantHireRequestDTO phrDTO,
			@PathVariable Long id, @RequestHeader HttpHeaders request) throws Exception {
		authService.authenticateRequestForRole(request, Role.WORKS_ENGINEER);

		PlantHireRequestDTO phr = rentalService.assessPlantHireRequest(phrDTO, id);

		PurchaseOrderDTO poDto = PurchaseOrderDTO.of(null, null, null, null, null, null, null);
		if (phr.getStatus() == Approval.APPROVED) {
			PlantInventoryEntryDTO plant = PlantInventoryEntryDTO.of(null, null, null, null);
			plant.add(phr.getPlant().getLink("self"));
			PurchaseOrderDTO dto = PurchaseOrderDTO.of(null, plant, phr.getRentalPeriod(), null, null,
					phr.getSiteEngineer(), phr.getConstructionSite());

			try {
				dto.add(new ExtendedLink(
						linkTo(methodOn(PlantHireRequestRestController.class)
								.createPlantHireRequestInvoice(phr.get_id(), null)).toString(),
						"sendInvoice", org.springframework.http.HttpMethod.POST));
				dto.add(new ExtendedLink(
						linkTo(methodOn(PlantHireRequestRestController.class)
								.setPlantHireRequestInvoiceLate(phr.get_id())).toString(),
						"setLatePayment", org.springframework.http.HttpMethod.PATCH));
			} catch (Exception e) {
				e.printStackTrace();
			}

			poDto = rentalService.createPurchaseOrder(dto);
			rentalService.associatePOtoPHR(poDto, id);
		}
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrderDTO>(poDto, headers, HttpStatus.NO_CONTENT);
	}

	@ExceptionHandler(PHRAcceptedException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public void handPlantNotFoundException(PHRAcceptedException ex) {

	}

	public class PHRAcceptedException extends Exception {
		private static final long serialVersionUID = 1L;

		public PHRAcceptedException(Long id) {
			super(String.format("PHR already accepted! (id: %d)", id));
		}
	}

	@ExceptionHandler(UnauthorizedException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public String handleNotFound(Exception exc) {
		return exc.getMessage();
	}

}
