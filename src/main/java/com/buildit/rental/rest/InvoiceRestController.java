package com.buildit.rental.rest;

import com.buildit.auth.application.exceptions.UnauthorizedException;
import com.buildit.auth.application.service.AuthService;
import com.buildit.auth.domain.model.Role;
import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.domain.model.Approval;
import com.buildit.common.rest.ExtendedLink;
import com.buildit.rental.application.dto.InvoiceDTO;

import com.buildit.rental.application.service.InvoiceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/invoices")
public class InvoiceRestController {

	@Autowired
	InvoiceService invoiceService;
	
	@Autowired
	AuthService authService;

	@CrossOrigin
	@RequestMapping(method = GET, path = "/")
	public ResponseEntity<List<InvoiceDTO>> getInvoices() throws Exception {
		List<InvoiceDTO> dto = invoiceService.findAllInvoices();
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<List<InvoiceDTO>>(dto, headers, HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(method = PATCH, path = "/{id}")
	public ResponseEntity<InvoiceDTO> assessInvoice(@RequestBody InvoiceDTO invoiceDTO, @PathVariable Long id, @RequestHeader HttpHeaders request) throws Exception {
		authService.authenticateRequestForRole(request, Role.SITE_ENGINEER);
		InvoiceDTO dto = invoiceService.assessInvoice(id, invoiceDTO.getStatus());
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<InvoiceDTO>(dto, headers, HttpStatus.OK);
	}
	
	@ExceptionHandler(UnauthorizedException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public String handleNotFound(Exception exc) {
		return exc.getMessage();
	}

}
