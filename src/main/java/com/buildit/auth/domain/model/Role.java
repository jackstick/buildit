package com.buildit.auth.domain.model;

public enum Role {
	
	SITE_ENGINEER,
	WORKS_ENGINEER

}
