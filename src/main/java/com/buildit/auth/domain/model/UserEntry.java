package com.buildit.auth.domain.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class UserEntry {
    @EmbeddedId
    UserID id;

    String username;
    String password;
    
    @Enumerated(EnumType.STRING)
    Role role;

}