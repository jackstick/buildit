package com.buildit.auth.domain.repository;

import com.buildit.auth.domain.model.UserEntry;
import com.buildit.auth.domain.model.UserID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository  extends JpaRepository<UserEntry, UserID> {
	
	UserEntry findByUsername(String username);
	
}