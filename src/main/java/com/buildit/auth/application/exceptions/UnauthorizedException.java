package com.buildit.auth.application.exceptions;

public class UnauthorizedException extends Exception {
    public UnauthorizedException(String msg) {
        super(msg);
    }
}
