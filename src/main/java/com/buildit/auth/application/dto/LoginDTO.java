package com.buildit.auth.application.dto;

import com.buildit.common.rest.ResourceSupport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName="of")
@NoArgsConstructor(force = true)
public class LoginDTO extends ResourceSupport {

    String username;
    String password;

}