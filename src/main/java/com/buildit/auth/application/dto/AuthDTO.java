package com.buildit.auth.application.dto;

import com.buildit.auth.domain.model.Role;
import com.buildit.common.rest.ResourceSupport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName="of")
@NoArgsConstructor(force = true)
public class AuthDTO extends ResourceSupport {

	Boolean authenticated;
	Role role;

}