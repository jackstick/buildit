package com.buildit.auth.application.service;

import com.buildit.auth.application.dto.AuthDTO;
import com.buildit.auth.application.exceptions.UnauthorizedException;
import com.buildit.auth.domain.model.Role;
import com.buildit.auth.domain.model.UserEntry;
import com.buildit.auth.domain.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

	@Autowired
	UserRepository userRepo;

	public AuthDTO authenticateUser(String username, String password) {

		UserEntry user = userRepo.findByUsername(username.toLowerCase());

		if (user == null) {
			return AuthDTO.of(false, null);
		}

		if (user.getPassword().equals(password)) {
			return AuthDTO.of(true, user.getRole());
		}

		return AuthDTO.of(false, null);
	}

	public boolean authenticateUserForRole(String username, String password, List<Role> roles) {

		AuthDTO auth = authenticateUser(username, password);

		if (auth.getAuthenticated()) {
			for (Role role : roles) {
				if (auth.getRole() == role) {
					return true;
				}
			}
		}
		return false;

	}

	public void authenticateRequestForRole(HttpHeaders request, List<Role> role) throws UnauthorizedException {

		if (!request.containsKey("username"))
			throw new UnauthorizedException("No username received.");
		if (!request.containsKey("password"))
			throw new UnauthorizedException("No password received.");

		String username = request.get("username").get(0);
		String password = request.get("password").get(0);

		if (!authenticateUserForRole(username, password, role))
			throw new UnauthorizedException("Invalid login.");

	}

	public void authenticateRequestForRole(HttpHeaders request, Role role) throws UnauthorizedException {

		if (!request.containsKey("username"))
			throw new UnauthorizedException("No username received.");
		if (!request.containsKey("password"))
			throw new UnauthorizedException("No password received.");

		String username = request.get("username").get(0);
		String password = request.get("password").get(0);

		List<Role> roleList = new ArrayList<>();
		roleList.add(role);

		if (!authenticateUserForRole(username, password, roleList))
			throw new UnauthorizedException("Invalid login.");

	}

}