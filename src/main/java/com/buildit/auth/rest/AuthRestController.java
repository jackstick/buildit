package com.buildit.auth.rest;

import com.buildit.auth.application.dto.AuthDTO;
import com.buildit.auth.application.dto.LoginDTO;
import com.buildit.auth.application.exceptions.UnauthorizedException;
import com.buildit.auth.application.service.AuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/login")
public class AuthRestController {

	@Autowired
	AuthService authService;

	@CrossOrigin
	@RequestMapping(method = POST, path = "/")
	public ResponseEntity<AuthDTO> login(@RequestBody LoginDTO loginDTO) throws Exception {
		AuthDTO authentication = authService.authenticateUser(loginDTO.getUsername(), loginDTO.getPassword());
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<AuthDTO>(authentication, headers, HttpStatus.OK);
	}
	
	@ExceptionHandler(UnauthorizedException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public String handleNotFound(Exception exc) {
		return exc.getMessage();
	}

}
