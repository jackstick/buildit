package com.buildit.common.domain.model;

public enum Approval {
	APPROVED,
	REJECTED,
	PENDING
}
