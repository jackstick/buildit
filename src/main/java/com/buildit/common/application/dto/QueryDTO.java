package com.buildit.common.application.dto;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import lombok.Data;

@Data
public class QueryDTO {
    String name;
    BusinessPeriodDTO rentalPeriod;
}
