package com.buildit.common.application.exceptions;

public class PlantNotAvailableException extends Exception {
    public PlantNotAvailableException(String msg) {
        super(msg);
    }
}