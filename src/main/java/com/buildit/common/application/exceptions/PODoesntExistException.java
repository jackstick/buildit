package com.buildit.common.application.exceptions;

public class PODoesntExistException extends Exception {
    public PODoesntExistException(String msg) {
        super(msg);
    }
}